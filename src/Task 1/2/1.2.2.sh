#!/bin/bash

json=$(cat ~/dst-stu/d/friends.json | jq .friends)

echo digraph G{ > graph.dot
iter=-1
for i in $(echo $json | jq)
do
  if [[ $i == \{ ]]
  then
    iter=$iter+1
    tmp=$(echo $json | jq .[$iter].music | perl -p -e 's/[\-]//g')
    name=$(echo $json | jq .[$iter].name)

    for num in $tmp
    do
      if [[ $num != [ && $num != ] ]]
      then
        echo '  '$name \-\> $num| tr -d \",  >> graph.dot
      fi
    done
  fi
done
echo }  >> graph.dot

dot -T png graph.dot > graph.png