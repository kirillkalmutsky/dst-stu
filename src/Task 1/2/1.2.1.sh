#!/bin/bash

cat ~/dst-stu/d/friends.json | 
jq .friends[] | 
jq '[.id, .name, .knows[]]' | 
jq 'join(" ")' | 
tr '"' ' ' | 
xargs -n4 sh -c 'echo "$0;$1;$2\n$0;$1;$3"' | 
awk -f ~/dst-stu/src/csv-to-tree.awk | 
dot -T png > relation.png 