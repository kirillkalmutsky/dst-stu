#!/usr/bin/env bash

root="/users/skyreaper/dst-stu/"
input="$root/d/mr/tf-idf/*"
output="./output.tsv"

cat $input | ./mapper.sh | sort | ./reducer.sh > $output