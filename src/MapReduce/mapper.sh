#!/usr/bin/env python

import sys
import re


for line in sys.stdin:
  words = re.sub("[.,?!'*^%$#:;/]", " ", line).lower().split()

  for word in words:
    print("%s\t%d" % (word, 1))