#!/usr/bin/env python

import sys

curr_word = None
aggregated_value = 0

for line in sys.stdin:
  word, value = line.strip().split("\t", 1)
  try:
    value = int(value)
  except ValueError:
    print('Some mistakes in mapper occured')
    continue

  if curr_word == word:
    aggregated_value += 1
    continue
  else:
    if curr_word is not None:
      print("%s\t%d" % (curr_word, aggregated_value))
    aggregated_value = value
    curr_word = word


if curr_word is not None:
  print("%s\t%d" % (curr_word, aggregated_value))