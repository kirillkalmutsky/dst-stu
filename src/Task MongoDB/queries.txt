All the results can be found in database: buax1aiwspzvpc2


query 1: 	
db.createCollection("tickets")


query 2: 
db.tickets.insertMany([
    {
        from: "Novosibirsk",
        to: "Moscow",
        price: "200$",
        passenger: 'Ivan'
    },
    {
        from: "Moscow",
        to: "London",
        price: "500$",
        passenger: ['Jhon', 'Swane'],
    },
    {
        from: "Iskitim",
        to: "Nowhere",
        price: "0$",
    }
])


query 3:
db.getCollection('tickets').find({})


query 4:
To get second element:
db.getCollection('tickets').find({}).skip(2).limit(1)

To get first and second elements:
db.getCollection('tickets').find({}).skip(0).limit(2)


query 5:
db.getCollection('tickets').find(ObjectId("5ed7941f6c9b639dc4ca0e50"))
