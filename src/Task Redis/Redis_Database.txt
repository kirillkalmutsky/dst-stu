127.0.0.1:6379> INFO keyspace
# Keyspace
db0:keys=16,expires=0,avg_ttl=0
127.0.0.1:6379> MOVE allnews 2
(integer) 1
127.0.0.1:6379> INFO keyspace
# Keyspace
db0:keys=15,expires=0,avg_ttl=0
db2:keys=1,expires=0,avg_ttl=0
127.0.0.1:6379> SMEMBERS allnews
(empty list or set)
127.0.0.1:6379> SELECT 2
OK
127.0.0.1:6379[2]> SMEMBERS allnews
1) "2 may"
2) "1 june"
3) "3 march"
4) "3 april"
5) "10 july"
