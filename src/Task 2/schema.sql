CREATE TABLE books (
  index INTEGER PRIMARY KEY,
  title TEXT,
  title_slug TEXT,
  isbn13 TEXT,
  isbn10 TEXT,
  price FLOAT,
  format TEXT,
  publisher TEXT,
  pubdate TEXT,
  edition TEXT,
  lexile TEXT,
  pages FLOAT,
  overview TEXT,
  excerpt TEXT,
  synopsis TEXT,
  toc TEXT,
  editorial_reviews TEXT,
  width FLOAT,
  height FLOAT,
  depth FLOAT
);

CREATE TABLE author(
  index        INTEGER PRIMARY KEY,
  author       TEXT,
  author_bio   TEXT,
  author_slug  TEXT
);


CREATE TABLE book2author(
  index         INTEGER PRIMARY KEY,
  book_index    INTEGER REFERENCES books(index),
  author_index  INTEGER REFERENCES author(index),
  is_main	INTEGER
);


CREATE TABLE subject(
  index    INTEGER PRIMARY KEY,
  subject  TEXT
);


CREATE TABLE book2subject(
  index          INTEGER PRIMARY KEY,
  book_index     INTEGER REFERENCES books(index),
  subject_index  INTEGER REFERENCES subject(index)
);