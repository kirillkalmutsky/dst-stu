SELECT auth.author, count(*)
FROM book2author AS b2a
JOIN author AS auth ON auth.index = b2a.author_index
GROUP BY auth.author
ORDER BY count(*) DESC
LIMIT 10;